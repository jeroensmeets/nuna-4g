package com.jeroensmeets.nuna4g;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import android.util.Log;

public class StatusActivity extends Activity {

    public static StatusActivity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        instance = this;
    }

    @Override
    public void onResume() {
        super.onResume();
        instance = this;
    }

    @Override
    public void onPause() {
        super.onPause();
        instance = null;
    }

    public void setConnectionStatusBlock(int statuscode) {

        int tvID;
        TextView tv;

        for (int i=0; i<4; i++) {
            tvID = getResources().getIdentifier( "statusLed" + String.valueOf( i ), "id", "com.jeroensmeets.nuna4g" );
            tv = (TextView) findViewById( tvID );

            if ( i == statuscode ) {
                tv.setBackgroundColor( Color.WHITE );
            } else {
                tv.setBackgroundColor( Color.BLACK );
            }
        }

    }

    public void setConnectionStatusLine(String status) {

        TextView tv = (TextView) findViewById(R.id.tv_connectionstatus);
        tv.setText( status );

        Log.d( "CONNECTIONSTATUS", status );

    }

    public void setUsbStatusLine(String status) {

        TextView tv = (TextView) findViewById(R.id.tv_usbstatus);
        tv.setText( status );

        Log.d( "USBSTATUS", status );

    }

}
