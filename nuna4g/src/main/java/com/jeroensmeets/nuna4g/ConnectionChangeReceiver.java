package com.jeroensmeets.nuna4g;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by jeroensmeets on 01-10-15.
 *
 * statusses for connection speed
 *
 * 0 : no connection
 * 1 : slow mobile data
 * 2 : fast mobile data
 * 3 : wifi
 *
 */

public class ConnectionChangeReceiver extends BroadcastReceiver {

    private GetConnectivitySpeed conn;

    @Override
    public void onReceive( Context context, Intent intent ) {

        int cs = getConnectionStatus(context);

        // todo: send int over USB to Arduino
        // sendCodeOverUSB( context, cs );

        if ( StatusActivity.instance != null ) {

            StatusActivity.instance.setConnectionStatusBlock( cs );

            String csString = "";
            switch (cs) {
                case 0:
                    csString = "Connection: none";
                    break;
                case 1:
                    csString = "Connection: slow mobile data";
                    break;
                case 2:
                    csString = "Connection: fast mobile data";
                    break;
                case 3:
                    csString = "Connection: wifi";
                    break;
            }

            StatusActivity.instance.setConnectionStatusLine(csString);

        }

    }

    private int getConnectionStatus( Context c ) {

        if ( conn == null ) {
            conn = new GetConnectivitySpeed();
        }

        if ( conn.isConnected( c )) {

            if (conn.isConnectedWifi( c )) {

                // addStatus("connection: wifi");
                return 3;

            } else {

                if (conn.isConnectedFast( c )) {
                    // addStatus("connection: mobile (fast)");
                    return 2;
                } else {
                    // addStatus("connection: mobile (slow)");
                    return 1;
                }

            }

        } else {
            // addStatus("connection: none");
            return 0;
        }

    }

//    private void sendCodeOverUSB( Context c, int code ) {
//        // Find all available drivers from attached devices.
//        UsbManager manager = (UsbManager) c.getSystemService(Context.USB_SERVICE);
//        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);
//        if (availableDrivers.isEmpty()) {
//            Log.d( "USBCONNECTION", "no usb serial drivers to usb device" );
//            if ( StatusActivity.instance != null ) {
//                StatusActivity.instance.setUsbStatusLine("No USB serial drivers found");
//            }
//            return;
//        }
//
//        // Open a connection to the first available driver.
//        UsbSerialDriver driver = availableDrivers.get(0);
//        UsbDeviceConnection connection = manager.openDevice(driver.getDevice());
//        if (connection == null) {
//            // You probably need to call UsbManager.requestPermission(driver.getDevice(), ..)
//            // UsbManager.requestPermission( driver.getDevice(), i );
//            if ( StatusActivity.instance != null ) {
//                StatusActivity.instance.setUsbStatusLine("No connection to USB device found");
//            }
//            Log.d( "USBCONNECTION", "no connection to usb device" );
//            return;
//        }
//
//        // Read some data! Most have just one port (port 0).
//        UsbSerialPort port = driver.getPorts().get(0);
//
//        try {
//            // need to send it as byte[]
//            port.write( BigInteger.valueOf(code).toByteArray(), 1000 );
//        } catch (IOException e) {
//            if ( StatusActivity.instance != null ) {
//                StatusActivity.instance.setUsbStatusLine("Error writing to USB device");
//            }
//            // Deal with error.
//        } finally {
//            try {
//                port.close();
//            } catch (IOException e) {
//
//            }
//        }
//
//    }

}
